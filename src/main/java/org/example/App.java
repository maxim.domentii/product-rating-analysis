package org.example;

import org.example.productratinganalysis.ProductRatingAnalysis;
import org.example.productratinganalysis.ProductRatingAnalysisImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/**
 * Main class to run and test the implementation of the ProductRatingAnalysis interface *
 */
public class App {

    public static void main( String[] args ) {
        // Validate input arguments
        if (args.length != 2) {
            System.out.println("Usage: java -jar target/product-rating-analysis-1.0-SNAPSHOT.jar <path_to_csv_file> <folder_path_for_output_json>");
            System.exit(1);
        }

        // Check if file exist at given path
        if (!new File(args[0]).exists()) {
            System.out.println("File not found on path " + args[0]);
            System.exit(1);
        }

        // Check if folder for output exist or create it otherwise
        File folder = new File(args[1]);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        // Get the input stream from the file and call productRatingAnalysis::calculate
        String pathToInputFile = args[0];
        String pathToOutputFile = folder.getAbsolutePath() + "/output.json";
        ProductRatingAnalysis productRatingAnalysis = new ProductRatingAnalysisImpl();
        try {
            FileInputStream inputStream = null;
            FileOutputStream outputStream = null;
            try {
                inputStream = new FileInputStream(pathToInputFile);

                Instant start = Instant.now();

                String resultJson = productRatingAnalysis.calculate(inputStream);

                Instant end = Instant.now();
                System.out.println("Processing took: " + Duration.between(start, end));

                outputStream = new FileOutputStream(pathToOutputFile);
                outputStream.write(resultJson.getBytes());
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e.fillInStackTrace());
        }
    }
}
