package org.example.productratinganalysis.utils;

public class ValidationUtils {

    public static boolean isNumeric(String s) {
        if (s == null || s.length() == 0){
            return false;
        }
        for (char c : s.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAlphanumeric(String s) {
        if (s == null || s.length() == 0){
            return false;
        }
        for (char c : s.toCharArray()) {
            if (!Character.isLetterOrDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAlphanumericWithHyphen(String s) {
        if (s == null || s.length() == 0){
            return false;
        }
        for (char c : s.toCharArray()) {
            if (!Character.isLetterOrDigit(c) && c != '-') {
                return false;
            }
        }
        return true;
    }
}
