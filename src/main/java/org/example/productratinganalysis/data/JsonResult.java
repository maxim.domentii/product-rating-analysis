package org.example.productratinganalysis.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

public class JsonResult {
    private int validLines;
    private int invalidLines;
    private List<String> bestRatedProducts;
    private List<String> worstRatedProducts;
    private String mostRatedProduct;
    private String lessRatedProduct;

    public JsonResult() {

    }

    public int getValidLines() {
        return validLines;
    }

    public void setValidLines(int validLines) {
        this.validLines = validLines;
    }

    public int getInvalidLines() {
        return invalidLines;
    }

    public void setInvalidLines(int invalidLines) {
        this.invalidLines = invalidLines;
    }

    public List<String> getBestRatedProducts() {
        return bestRatedProducts;
    }

    public void setBestRatedProducts(List<String> bestRatedProducts) {
        this.bestRatedProducts = bestRatedProducts;
    }

    public List<String> getWorstRatedProducts() {
        return worstRatedProducts;
    }

    public void setWorstRatedProducts(List<String> worstRatedProducts) {
        this.worstRatedProducts = worstRatedProducts;
    }

    public String getMostRatedProduct() {
        return mostRatedProduct;
    }

    public void setMostRatedProduct(String mostRatedProduct) {
        this.mostRatedProduct = mostRatedProduct;
    }

    public String getLessRatedProduct() {
        return lessRatedProduct;
    }

    public void setLessRatedProduct(String lessRatedProduct) {
        this.lessRatedProduct = lessRatedProduct;
    }

    public static final class JsonResultBuilder {
        private int validLines;
        private int invalidLines;
        private List<String> bestRatedProducts;
        private List<String> worstRatedProducts;
        private String mostRatedProduct;
        private String lessRatedProduct;

        private JsonResultBuilder() {
        }

        public static JsonResultBuilder aJsonResult() {
            return new JsonResultBuilder();
        }

        public JsonResultBuilder withValidLines(int validLines) {
            this.validLines = validLines;
            return this;
        }

        public JsonResultBuilder withInvalidLines(int invalidLines) {
            this.invalidLines = invalidLines;
            return this;
        }

        public JsonResultBuilder withBestRatedProducts(List<String> bestRatedProducts) {
            this.bestRatedProducts = bestRatedProducts;
            return this;
        }

        public JsonResultBuilder withWorstRatedProducts(List<String> worstRatedProducts) {
            this.worstRatedProducts = worstRatedProducts;
            return this;
        }

        public JsonResultBuilder withMostRatedProduct(String mostRatedProduct) {
            this.mostRatedProduct = mostRatedProduct;
            return this;
        }

        public JsonResultBuilder withLessRatedProduct(String lessRatedProduct) {
            this.lessRatedProduct = lessRatedProduct;
            return this;
        }

        public JsonResult build() {
            JsonResult jsonResult = new JsonResult();
            jsonResult.setValidLines(validLines);
            jsonResult.setInvalidLines(invalidLines);
            jsonResult.setBestRatedProducts(bestRatedProducts);
            jsonResult.setWorstRatedProducts(worstRatedProducts);
            jsonResult.setMostRatedProduct(mostRatedProduct);
            jsonResult.setLessRatedProduct(lessRatedProduct);
            return jsonResult;
        }
    }
}
