package org.example.productratinganalysis.data;

public class CsvRecord {

    private String buyerId;
    private String shopId;
    private String productId;
    private int rating;

    public String getBuyerId() {
        return buyerId;
    }

    public String getShopId() {
        return shopId;
    }

    public String getProductId() {
        return productId;
    }

    public int getRating() {
        return rating;
    }

    public static final class CsvRecordBuilder {
        private String buyerId;
        private String shopId;
        private String productId;
        private int rating;

        private CsvRecordBuilder() {
        }

        public static CsvRecordBuilder aCsvRecord() {
            return new CsvRecordBuilder();
        }

        public CsvRecordBuilder withBuyerId(String buyerId) {
            this.buyerId = buyerId;
            return this;
        }

        public CsvRecordBuilder withShopId(String shopId) {
            this.shopId = shopId;
            return this;
        }

        public CsvRecordBuilder withProductId(String productId) {
            this.productId = productId;
            return this;
        }

        public CsvRecordBuilder withRating(int rating) {
            this.rating = rating;
            return this;
        }

        public CsvRecord build() {
            CsvRecord csvRecord = new CsvRecord();
            csvRecord.buyerId = this.buyerId;
            csvRecord.rating = this.rating;
            csvRecord.productId = this.productId;
            csvRecord.shopId = this.shopId;
            return csvRecord;
        }
    }
}
