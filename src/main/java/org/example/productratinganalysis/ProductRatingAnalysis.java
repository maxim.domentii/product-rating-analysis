package org.example.productratinganalysis;

/**
 * Interface to calculate the product rating analysis from a given input stream of order records with ratings
 */
public interface ProductRatingAnalysis {

    /**
     * Given an input stream of order records with rating validates the records and calculates:
     * <ul>
     * <li/>Count of valid input processed
     * <li/>Count of invalid inputs
     * <li/>Top 3 best rated products
     * <li/>Top 3 worst rated products
     * <li/>Most rated product
     * <li/>Lest rated product
     * <ul/>
     * @param in is the input stream of order records
     * @return the analysis as a String JSON
     */
    String calculate(java.io.InputStream in);
}
