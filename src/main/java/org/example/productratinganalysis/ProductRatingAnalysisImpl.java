package org.example.productratinganalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.productratinganalysis.data.CsvRecord;
import org.example.productratinganalysis.data.JsonResult;
import org.example.productratinganalysis.utils.ValidationUtils;

import java.io.InputStream;
import java.util.*;

public class ProductRatingAnalysisImpl implements ProductRatingAnalysis {

    private ObjectMapper objectMapper;
    private ThreadLocal<Integer> validProcessedInputLocal;
    private ThreadLocal<Integer> invalidInputLocal;
    private ThreadLocal<Map<String, int[]>> productRatingMapLocal;

    public ProductRatingAnalysisImpl() {
        this.objectMapper = new ObjectMapper();
        this.validProcessedInputLocal = ThreadLocal.withInitial(() -> 0);
        this.invalidInputLocal = ThreadLocal.withInitial(() -> 0);
        this.productRatingMapLocal = ThreadLocal.withInitial(HashMap::new);
    }

    @Override
    public String calculate(InputStream in) {
        try {
            Scanner sc = null;
            try {
                sc = new Scanner(in, "UTF-8");

                // skip header line
                sc.nextLine();

                while (sc.hasNextLine()) {
                    String line = sc.nextLine();

                    CsvRecord record = parseAndValidate(line);
                    if (record != null){
                        validProcessedInputLocal.set(validProcessedInputLocal.get() + 1);

                        Map<String, int[]> productRatingMap = productRatingMapLocal.get();
                        int[] ratingSumAndCount = productRatingMap.get(record.getProductId());
                        if (ratingSumAndCount != null) {
                            ratingSumAndCount[0] += record.getRating();
                            ratingSumAndCount[1]++;
                        } else {
                            productRatingMap.put(record.getProductId(), new int[]{record.getRating(), 1});
                        }
                    } else {
                        invalidInputLocal.set(invalidInputLocal.get() + 1);
                    }
                }

                // note that Scanner suppresses exceptions
                if (sc.ioException() != null) {
                    throw sc.ioException();
                }
            } finally {
                if (sc != null) {
                    sc.close();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e.fillInStackTrace());
        }

        String[] mostLestRatings = getCountMostAndLessRatedProducts();
        JsonResult jsonResult = JsonResult.JsonResultBuilder.aJsonResult()
                .withValidLines(validProcessedInputLocal.get())
                .withInvalidLines(invalidInputLocal.get())
                .withBestRatedProducts(getTop3RatedProducts(true))
                .withWorstRatedProducts(getTop3RatedProducts(false))
                .withMostRatedProduct(mostLestRatings[0])
                .withLessRatedProduct(mostLestRatings[1])
                .build();

        try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonResult);
        } catch (Exception e) {
            throw new RuntimeException(e.fillInStackTrace());
        }
    }

    private String[] getCountMostAndLessRatedProducts() {
        String[] result = new String[2];
        int minCount = Integer.MAX_VALUE;
        int maxCount = Integer.MIN_VALUE;

        for (Map.Entry<String, int[]> entry : productRatingMapLocal.get().entrySet()) {
            int ratingCounts = entry.getValue()[1];
            String productId = entry.getKey();
            if (ratingCounts > maxCount) {
                maxCount = ratingCounts;
                result[0] = productId;
            }
            if (ratingCounts < minCount) {
                minCount = ratingCounts;
                result[1] = productId;
            }
        }
        return result;
    }

    private List<String> getTop3RatedProducts(boolean best) {
        PriorityQueue<Object[]> queue = new PriorityQueue<>(3,
                Comparator.comparingDouble(a -> best ? (double) a[1] : -(double) a[1]));

        for (Map.Entry<String, int[]> entry : productRatingMapLocal.get().entrySet()) {
            double avgRating = entry.getValue()[0] * 1.0 / entry.getValue()[1];
            String productId = entry.getKey();
            queue.offer(new Object[]{productId, avgRating});
            if (queue.size() > 3) {
                queue.poll();
            }
        }

        List<String> result = new LinkedList<>();
        while (!queue.isEmpty()) {
            result.add((String)queue.poll()[0]);
        }
        Collections.reverse(result);
        return result;
    }

    private CsvRecord parseAndValidate(String line) {
        String[] columns = line.split(",");

        // Validate row length
        if (columns.length != 4) {
            return null;
        }

        // Validate BuyerId
        String buyerId = columns[0];
        if (buyerId.length() == 0 || !Character.isLetter(buyerId.charAt(0))
                || !ValidationUtils.isAlphanumeric(buyerId)) {
            return null;
        }

        // Validate ShopId
        String shopId = columns[1];
        if (shopId.length() == 0 || !Character.isLetter(shopId.charAt(0))
                || !ValidationUtils.isAlphanumeric(shopId)) {
            return null;
        }

        // Validate ProductId
        String productId = columns[2];
        if (productId.length() == 0 || !Character.isLetter(productId.charAt(0))
                || !ValidationUtils.isAlphanumericWithHyphen(productId)
                || productId.lastIndexOf("-") < 0
                || !ValidationUtils.isNumeric(productId.substring(productId.lastIndexOf("-") + 1))) {
            return null;
        }

        // Validate Rating
        String rating = columns[3];
        if (rating.length() == 0 || !ValidationUtils.isNumeric(rating)) {
            return null;
        }

        return CsvRecord.CsvRecordBuilder.aCsvRecord()
                .withBuyerId(buyerId)
                .withShopId(shopId)
                .withProductId(productId)
                .withRating(Integer.parseInt(rating))
                .build();
    }
}
