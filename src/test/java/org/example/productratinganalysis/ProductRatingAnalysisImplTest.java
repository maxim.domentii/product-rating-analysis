package org.example.productratinganalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.productratinganalysis.data.JsonResult;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ProductRatingAnalysisImplTest {

    private ObjectMapper objectMapper;
    private ProductRatingAnalysis productRatingAnalysis;

    @Before
    public void setUp() {
        this.objectMapper = new ObjectMapper();
        this.productRatingAnalysis = new ProductRatingAnalysisImpl();
    }

    @Test
    public void testCalculateValidationFailForEmpty() throws IOException {
        // given
        InputStream inputStream = new ByteArrayInputStream(
                ("Buyer Id,Shop Id,Product Id,Rating\n" +
                        "\n").getBytes());

        // when
        String result = productRatingAnalysis.calculate(inputStream);

        // then
        JsonResult jsonResult = objectMapper.readValue(result, JsonResult.class);
        assertEquals(1, jsonResult.getInvalidLines());
        assertEquals(0, jsonResult.getValidLines());
    }

    @Test
    public void testCalculateValidationFailForInvalidBuyerId() throws IOException {
        // given
        InputStream inputStream = new ByteArrayInputStream(
                ("Buyer Id,Shop Id,Product Id,Rating\n" +
                        "1buyer,shop1,product-1,5\nbuyer?,shop1,product-1,5").getBytes());

        // when
        String result = productRatingAnalysis.calculate(inputStream);

        // then
        JsonResult jsonResult = objectMapper.readValue(result, JsonResult.class);
        assertEquals(2, jsonResult.getInvalidLines());
        assertEquals(0, jsonResult.getValidLines());
    }

    @Test
    public void testCalculateValidationFailForInvalidShopId() throws IOException {
        // given
        InputStream inputStream = new ByteArrayInputStream(
                ("Buyer Id,Shop Id,Product Id,Rating\n" +
                        "buyer1,1shop,product-1,5\nbuyer1,shop_,product-1,5").getBytes());

        // when
        String result = productRatingAnalysis.calculate(inputStream);

        // then
        JsonResult jsonResult = objectMapper.readValue(result, JsonResult.class);
        assertEquals(2, jsonResult.getInvalidLines());
        assertEquals(0, jsonResult.getValidLines());
    }

    @Test
    public void testCalculateValidationFailForInvalidProductId() throws IOException {
        // given
        InputStream inputStream = new ByteArrayInputStream(
                ("Buyer Id,Shop Id,Product Id,Rating\n" +
                        "buyer1,shop1,1product-1,5\nbuyer1,shop1,product-,5\nbuyer1,shop1,prod?uct-1,5").getBytes());

        // when
        String result = productRatingAnalysis.calculate(inputStream);

        // then
        JsonResult jsonResult = objectMapper.readValue(result, JsonResult.class);
        assertEquals(3, jsonResult.getInvalidLines());
        assertEquals(0, jsonResult.getValidLines());
    }

    @Test
    public void testCalculateValidationFailForInvalidRating() throws IOException {
        // given
        InputStream inputStream = new ByteArrayInputStream(
                ("Buyer Id,Shop Id,Product Id,Rating\n" +
                        "buyer1,shop1,product-1,\nbuyer1,shop1,product-1,aa\nbuyer1,shop1,product-1,2b").getBytes());

        // when
        String result = productRatingAnalysis.calculate(inputStream);

        // then
        JsonResult jsonResult = objectMapper.readValue(result, JsonResult.class);
        assertEquals(3, jsonResult.getInvalidLines());
        assertEquals(0, jsonResult.getValidLines());
    }

    @Test
    public void testCalculateValidLinesAndMostRatedAndLessRated() throws IOException {
        // given
        InputStream inputStream = new ByteArrayInputStream(
                ("Buyer Id,Shop Id,Product Id,Rating\n" +
                        "buyer1,shop1,product-1,5\nbuyer1,shop1,product-1,5\nbuyer1,shop1,product-2,5").getBytes());

        // when
        String result = productRatingAnalysis.calculate(inputStream);

        // then
        JsonResult jsonResult = objectMapper.readValue(result, JsonResult.class);
        assertEquals(0, jsonResult.getInvalidLines());
        assertEquals(3, jsonResult.getValidLines());
        assertEquals("product-1", jsonResult.getMostRatedProduct());
        assertEquals("product-2", jsonResult.getLessRatedProduct());
    }

    @Test
    public void testCalculateValidLinesAndTopBestRatedAndTopWorstRated() throws IOException {
        // given
        InputStream inputStream = new ByteArrayInputStream(
                ("Buyer Id,Shop Id,Product Id,Rating\n" +
                        "buyer1,shop1,product-1,1\nbuyer1,shop1,product-2,5\n" +
                        "buyer1,shop1,product-3,4\nbuyer1,shop1,product-4,3").getBytes());

        // when
        String result = productRatingAnalysis.calculate(inputStream);

        // then
        JsonResult jsonResult = objectMapper.readValue(result, JsonResult.class);
        assertEquals(0, jsonResult.getInvalidLines());
        assertEquals(4, jsonResult.getValidLines());
        assertEquals(new ArrayList<>(Arrays.asList("product-2", "product-3", "product-4")),
                jsonResult.getBestRatedProducts());
        assertEquals(new ArrayList<>(Arrays.asList("product-1", "product-4", "product-3")),
                jsonResult.getWorstRatedProducts());
    }
}