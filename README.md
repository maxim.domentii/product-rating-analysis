## Product Rating Analysis

This is a java library that include the interface implementation of the ProductRatingInterface and a main class App
to test the implementation. In the test package are present the unit tests for the implemented solution.

For more details about the solution analysis, design and implementation please check PDF document in root folder
"Solution Documentation.pdf"

# Build
```shell
mvn clean package
```
# Run
```shell
java -jar target/product-rating-analysis-1.0-SNAPSHOT.jar <path_to_csv_file> <folder_path_for_output_json>
```
# Run unit tests
```shell
mvn verify
```